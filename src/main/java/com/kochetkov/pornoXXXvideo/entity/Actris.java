package com.kochetkov.pornoXXXvideo.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "actris")
public class Actris {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "experience")
    private int experience;

    @Column(name = "achievements")
    private String achievements;

    @Column(name = "category")
    private String category;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @OneToMany(cascade = {CascadeType.ALL})
    @JoinColumn(name = "actris_id")
    private List<Comment> comments;
}
