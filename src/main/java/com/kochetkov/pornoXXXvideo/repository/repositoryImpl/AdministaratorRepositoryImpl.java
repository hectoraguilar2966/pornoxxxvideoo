package com.kochetkov.pornoXXXvideo.repository.repositoryImpl;

import com.kochetkov.pornoXXXvideo.entity.Administrator;
import com.kochetkov.pornoXXXvideo.repository.AdministratorRepository;
import com.kochetkov.pornoXXXvideo.utill.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

import static com.kochetkov.pornoXXXvideo.utill.EntityManagerUtils.getEntityManager;

@Repository
public class AdministaratorRepositoryImpl implements AdministratorRepository {

    @Override
    public Administrator findByEmail(String email) {
        EntityManager em = getEntityManager();

        Administrator foundAdministrator = (Administrator) em.createNativeQuery(
                String.format("SELECT * FROM administrator WHERE email = \"%s\"", email), Administrator.class)
                .getSingleResult();

        em.close();

        System.out.println("Found: " + foundAdministrator);
        return foundAdministrator;
    }

    @Override
    public List<Administrator> findAll() {
        EntityManager em = getEntityManager();

        List<Administrator> foundList = em.createNativeQuery("SELECT * FROM administrator", Administrator.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() + " administrators");
        return foundList;
    }

    @Override
    public Administrator findById(Long id) {
        EntityManager em = getEntityManager();

        Administrator foundAdministrator = em.find(Administrator.class, id);

        em.close();
        System.out.println("Found administrator: " + foundAdministrator);
        return foundAdministrator;
    }

    @Override
    public Administrator create(Administrator administrator) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.persist(administrator);

        em.getTransaction().commit();
        em.close();
        System.out.println("Administrator was created. Id: " + administrator.getId());
        return administrator;
    }

    @Override
    public Administrator update(Administrator administrator) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.merge(administrator);

        em.getTransaction().commit();
        em.close();
        System.out.println("Administrator was updated. Id: " + administrator.getId());
        return administrator;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Administrator foundAdministrator = em.find(Administrator.class, id);
        em.remove(foundAdministrator);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        entityManager.getTransaction().begin();

        entityManager.createNativeQuery("DELETE FROM administrator ").executeUpdate();

        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
