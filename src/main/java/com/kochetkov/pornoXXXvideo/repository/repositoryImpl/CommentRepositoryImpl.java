package com.kochetkov.pornoXXXvideo.repository.repositoryImpl;

import com.kochetkov.pornoXXXvideo.entity.Comment;
import com.kochetkov.pornoXXXvideo.repository.CommentRepositiry;
import com.kochetkov.pornoXXXvideo.utill.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

import static com.kochetkov.pornoXXXvideo.utill.EntityManagerUtils.getEntityManager;

@Repository
public class CommentRepositoryImpl implements CommentRepositiry {

    @Override
    public List<Comment> findAll() {
        EntityManager em = getEntityManager();

        List<Comment> foundList = em.createNativeQuery("SELECT * FROM `comment`", Comment.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() + " comments");
        return foundList;
    }

    @Override
    public Comment findById(Long id) {
        EntityManager em = getEntityManager();

        Comment foundComment = em.find(Comment.class, id);

        em.close();
        System.out.println("Found comment: " + foundComment);
        return foundComment;
    }

    @Override
    public Comment create(Comment comment) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.persist(comment);

        em.getTransaction().commit();
        em.close();
        System.out.println("Comment was created. Id: " + comment.getId());
        return comment;
    }

    @Override
    public Comment update(Comment comment) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.merge(comment);

        em.getTransaction().commit();
        em.close();
        System.out.println("Comment was updated. Id: " + comment.getId());
        return comment;
    }

    @Override
    public List<Comment> findAllByActrisId(Long id) {
        EntityManager em = getEntityManager();

        List<Comment> foundList = em.createNativeQuery(
                String.format("SELECT * FROM comment WHERE acrtis_id = \"%s\"", id), Comment.class)
                .getResultList();

        em.close();
        return foundList;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Comment foundComment = em.find(Comment.class, id);
        em.remove(foundComment);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        entityManager.getTransaction().begin();

        entityManager.createNativeQuery("DELETE FROM comment ").executeUpdate();

        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
