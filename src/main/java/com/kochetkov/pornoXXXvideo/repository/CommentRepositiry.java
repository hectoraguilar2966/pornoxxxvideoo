package com.kochetkov.pornoXXXvideo.repository;

import com.kochetkov.pornoXXXvideo.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepositiry{

    List<Comment> findAll();

    Comment findById(Long id);

    Comment create(Comment comment);

    Comment update(Comment comment);

    void deleteById(Long id);

    void deleteAll();

    List<Comment> findAllByActrisId(Long id);
}
