package com.kochetkov.pornoXXXvideo.dto.administratorDto;

import com.kochetkov.pornoXXXvideo.entity.enums.Role;
import com.kochetkov.pornoXXXvideo.entity.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdministratorUpdateDto {

    private Long id;

    private String name;

    private String surname;

    private String email;

    private String password;

}
