package com.kochetkov.pornoXXXvideo.dto.administratorDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdministratorCreateDto {

    private String name;

    private String surname;

    private String email;

    private String password;

}
