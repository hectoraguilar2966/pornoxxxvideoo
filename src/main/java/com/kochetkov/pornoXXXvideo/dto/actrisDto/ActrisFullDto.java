package com.kochetkov.pornoXXXvideo.dto.actrisDto;

import com.kochetkov.pornoXXXvideo.entity.Comment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActrisFullDto {

    private Long id;

    private String name;

    private String surname;

    private String category;

    private int experience;

    private String achievements;

    private String email;

    private List<Comment> comments;
}
