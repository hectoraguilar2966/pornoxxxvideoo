package com.kochetkov.pornoXXXvideo.dto.actrisDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActrisUpdateDto {

    private Long id;

    private String name;

    private String surname;

    private String category;

    private int experience;

    private String achievements;

    private String password;

}
