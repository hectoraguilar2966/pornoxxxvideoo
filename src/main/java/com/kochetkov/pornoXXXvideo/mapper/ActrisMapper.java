package com.kochetkov.pornoXXXvideo.mapper;

import com.kochetkov.pornoXXXvideo.dto.actrisDto.ActrisCreateDto;
import com.kochetkov.pornoXXXvideo.dto.actrisDto.ActrisFullDto;
import com.kochetkov.pornoXXXvideo.dto.actrisDto.ActrisPreviewDto;
import com.kochetkov.pornoXXXvideo.dto.actrisDto.ActrisUpdateDto;
import com.kochetkov.pornoXXXvideo.entity.Actris;

import java.util.ArrayList;
import java.util.List;

public class ActrisMapper {

    public List<ActrisPreviewDto> mapToDtoList(List<Actris> entities) {
        List<ActrisPreviewDto> dtos = new ArrayList<>();

        for (Actris entity : entities) {
            ActrisPreviewDto dto = new ActrisPreviewDto();

            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setSurname(entity.getSurname());
            dto.setCategory(entity.getCategory());
            dto.setExperience(entity.getExperience());
            dto.setEmail(entity.getEmail());

            dtos.add(dto);
        }
        return dtos;
    }

    public Actris mapToEntity(ActrisCreateDto createDto) {
        Actris actris = new Actris();
        actris.setName(createDto.getName());
        actris.setSurname(createDto.getSurname());
        actris.setCategory(createDto.getCategory());
        actris.setAchievements(createDto.getAchievements());
        actris.setExperience(createDto.getExperience());
        actris.setPassword(createDto.getPassword());
        actris.setEmail(createDto.getEmail());

        return actris;
    }

    public Actris mapToEntity(ActrisUpdateDto updateDto) {
        Actris actris = new Actris();
        actris.setName(updateDto.getName());
        actris.setSurname(updateDto.getSurname());
        actris.setCategory(updateDto.getCategory());
        actris.setExperience(updateDto.getExperience());
        actris.setAchievements(updateDto.getAchievements());
        actris.setPassword(updateDto.getPassword());

        return actris;
    }

    public ActrisFullDto mapToDto(Actris entity) {
        ActrisFullDto fullDto = new ActrisFullDto();
        fullDto.setId(entity.getId());
        fullDto.setName(entity.getName());
        fullDto.setSurname(entity.getSurname());
        fullDto.setCategory(entity.getCategory());
        fullDto.setAchievements(entity.getAchievements());
        fullDto.setExperience(entity.getExperience());
        fullDto.setEmail(entity.getEmail());
        fullDto.setComments(entity.getComments());

        return fullDto;
    }
}
