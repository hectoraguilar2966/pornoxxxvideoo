package com.kochetkov.pornoXXXvideo.controller;

import com.kochetkov.pornoXXXvideo.dto.actrisDto.ActrisCreateDto;
import com.kochetkov.pornoXXXvideo.dto.actrisDto.ActrisFullDto;
import com.kochetkov.pornoXXXvideo.dto.actrisDto.ActrisLoginDto;
import com.kochetkov.pornoXXXvideo.dto.actrisDto.ActrisPreviewDto;
import com.kochetkov.pornoXXXvideo.dto.administratorDto.AdministratorLoginDto;
import com.kochetkov.pornoXXXvideo.dto.commentDto.CommentPreviewDto;
import com.kochetkov.pornoXXXvideo.entity.Actris;
import com.kochetkov.pornoXXXvideo.entity.Comment;
import com.kochetkov.pornoXXXvideo.repository.ActrisRepository;
import com.kochetkov.pornoXXXvideo.repository.CommentRepositiry;
import com.kochetkov.pornoXXXvideo.service.*;
import com.kochetkov.pornoXXXvideo.service.serviceImpl.ActrisServiceImpl;
import com.kochetkov.pornoXXXvideo.service.serviceImpl.AutActrisServisImpl;
import com.kochetkov.pornoXXXvideo.service.serviceImpl.AutAdministratorServiceImpl;
import com.kochetkov.pornoXXXvideo.service.serviceImpl.CommentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Controller
public class ActrisController {

    private ActrisService actrisService = new ActrisServiceImpl();

    private CommentService commentService = new CommentServiceImpl();

    private AutAdminsrtatorService autAdminsrtatorService = new AutAdministratorServiceImpl();

    private AutActrisService autActrisService = new AutActrisServisImpl();

    @Autowired
    private CommentRepositiry commentRepositiry;
    @Autowired
    private ActrisRepository actrisRepository;


    @RequestMapping(method = RequestMethod.GET, value = "/index")
    public String openMainPage(Model model) {
        List<ActrisPreviewDto> found = actrisService.findAll();

        model.addAttribute("all_actrises", found);
        return "index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/actris/{id}")
    public String openProfile(@PathVariable Long id, Model model) {
        ActrisFullDto foundActris = actrisService.findById(id);
        List<CommentPreviewDto> comments = commentService.findAllByActrisId(id);
        model.addAttribute("actris", foundActris);
        model.addAttribute("comments", comments);

        return "actris";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/registration")
    public String openCreateTrainer(Model model) {
        model.addAttribute("createDto", new ActrisCreateDto());

        return "create-actris";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/actris/create")
    public String saveActris(Actris actris) {
        Actris created = actrisRepository.create(actris);
//        created.setComments(new HashSet<>(Arrays.asList(new Comment())));

        Comment comment = new Comment();
        comment.setActris(created);
        comment.setMessage("mes");
        commentRepositiry.create(comment);

        return "redirect:/actris/" + created.getId();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/actris/sign-in")
    public String loginActris(Model model) {
        model.addAttribute("loginDto", new AdministratorLoginDto());

        return "actris-sign-in";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/actris/login")
    public String loginActris(ActrisLoginDto loginDto) {
        autActrisService.login(loginDto);

        return "redirect:/index-for-actris";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/index-for-actris")
    public String openMainPageByLoginedActris(Model model) {
        if (!autActrisService.isAuthenticated()) {
            return "redirect:/actris-invalid-login";
        }

        List<ActrisPreviewDto> found = actrisService.findAll();

        model.addAttribute("all_actrises", found);
        model.addAttribute("logined_actris", autActrisService.getLoginedTrainer());

        return "index-for-actris";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/actris-invalid-login")
    public String showActrisLoginException() {
        return "exception";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/actris/logout")
    public String logOut() {
        return "redirect:/index";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/actris/logout")
    public String logout() {
        autActrisService.logOut();
        return "redirect:/index";
    }
}
