package com.kochetkov.pornoXXXvideo.controller;

import com.kochetkov.pornoXXXvideo.dto.actrisDto.ActrisFullDto;
import com.kochetkov.pornoXXXvideo.dto.commentDto.CommentCreateDto;
import com.kochetkov.pornoXXXvideo.dto.commentDto.CommentFullDto;
import com.kochetkov.pornoXXXvideo.service.ActrisService;
import com.kochetkov.pornoXXXvideo.service.CommentService;
import com.kochetkov.pornoXXXvideo.service.serviceImpl.ActrisServiceImpl;
import com.kochetkov.pornoXXXvideo.service.serviceImpl.CommentServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CommentController {

    private CommentService commentService = new CommentServiceImpl();

    private ActrisService actrisService = new ActrisServiceImpl();


    @RequestMapping(method = RequestMethod.GET, value = "actris/comment/create/{id}")
    public String openCreationForm(@PathVariable Long id, Model model) {
        ActrisFullDto foundActris = actrisService.findById(id);
        model.addAttribute("actris", foundActris);
        model.addAttribute("createDto", new CommentCreateDto());

        return "create-comment";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/comment/create")
    public String saveRecording(CommentCreateDto createDto) {
        CommentFullDto created = commentService.create(createDto);

        return "redirect:/index";
    }
}
