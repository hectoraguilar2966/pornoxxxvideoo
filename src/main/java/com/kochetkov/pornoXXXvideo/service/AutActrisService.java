package com.kochetkov.pornoXXXvideo.service;

import com.kochetkov.pornoXXXvideo.dto.actrisDto.ActrisLoginDto;
import com.kochetkov.pornoXXXvideo.entity.Actris;

public interface AutActrisService {

    void login(ActrisLoginDto loginDto);

    void logOut();

    boolean isAuthenticated();

    Actris getLoginedTrainer();
}
