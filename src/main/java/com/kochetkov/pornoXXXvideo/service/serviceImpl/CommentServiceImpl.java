package com.kochetkov.pornoXXXvideo.service.serviceImpl;

import com.kochetkov.pornoXXXvideo.dto.commentDto.CommentCreateDto;
import com.kochetkov.pornoXXXvideo.dto.commentDto.CommentFullDto;
import com.kochetkov.pornoXXXvideo.dto.commentDto.CommentPreviewDto;
import com.kochetkov.pornoXXXvideo.dto.commentDto.CommentUpdateDto;
import com.kochetkov.pornoXXXvideo.entity.Actris;
import com.kochetkov.pornoXXXvideo.entity.Comment;
import com.kochetkov.pornoXXXvideo.exception.InvalidDtoException;
import com.kochetkov.pornoXXXvideo.exception.MissedUpdateldException;
import com.kochetkov.pornoXXXvideo.mapper.CommentMapper;
import com.kochetkov.pornoXXXvideo.repository.ActrisRepository;
import com.kochetkov.pornoXXXvideo.repository.CommentRepositiry;
import com.kochetkov.pornoXXXvideo.repository.repositoryImpl.ActrisRepositoryImpl;
import com.kochetkov.pornoXXXvideo.repository.repositoryImpl.CommentRepositoryImpl;
import com.kochetkov.pornoXXXvideo.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepositiry commentRepositiry = new CommentRepositoryImpl();

    @Autowired
    private ActrisRepository actrisRepository = new ActrisRepositoryImpl();

    private CommentMapper commentMapper = new CommentMapper();

    @Override
    public List<CommentPreviewDto> findAll() {
        List<Comment> found = commentRepositiry.findAll();

        return commentMapper.mapToDtoList(found);
    }

    @Override
    public CommentFullDto findById(Long id) {
        Comment found = commentRepositiry.findById(id);

        return commentMapper.mapToDto(found);
    }

    @Override
    public CommentFullDto create(CommentCreateDto createDto){
        Actris actris = actrisRepository.findById(createDto.getActrisId());
        Comment toSave = commentMapper.mapToEntity(createDto, actris);

        Comment created = commentRepositiry.create(toSave);

        return commentMapper.mapToDto(created);
    }

    @Override
    public CommentFullDto update(CommentUpdateDto updateDto){
        Comment toUpdate = commentMapper.mapToEntity(updateDto);
        Comment existingEntity = commentRepositiry.findById(updateDto.getId());

        toUpdate.setName(existingEntity.getName());
        toUpdate.setSurname(existingEntity.getSurname());
        toUpdate.setEmail(existingEntity.getEmail());
        toUpdate.setActris(existingEntity.getActris());

        Comment updated = commentRepositiry.update(toUpdate);

        return commentMapper.mapToDto(updated);
    }

    @Override
    public void deleteById(Long id) {
        commentRepositiry.deleteById(id);
        System.out.println("CommentServiceImpl -> comment with id " + id + " was deleted");
    }

    @Override
    public List<CommentPreviewDto> findAllByActrisId(Long id) {
        List<Comment> comments = commentRepositiry.findAllByActrisId(id);

        return commentMapper.mapToDtoList(comments);
    }
}
