package com.kochetkov.pornoXXXvideo.service.serviceImpl;

import com.kochetkov.pornoXXXvideo.dto.actrisDto.ActrisCreateDto;
import com.kochetkov.pornoXXXvideo.dto.actrisDto.ActrisFullDto;
import com.kochetkov.pornoXXXvideo.dto.actrisDto.ActrisPreviewDto;
import com.kochetkov.pornoXXXvideo.dto.actrisDto.ActrisUpdateDto;
import com.kochetkov.pornoXXXvideo.entity.Actris;
import com.kochetkov.pornoXXXvideo.mapper.ActrisMapper;
import com.kochetkov.pornoXXXvideo.repository.ActrisRepository;
import com.kochetkov.pornoXXXvideo.repository.repositoryImpl.ActrisRepositoryImpl;
import com.kochetkov.pornoXXXvideo.service.ActrisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActrisServiceImpl implements ActrisService {

    @Autowired
    private ActrisRepository actrisRepository = new ActrisRepositoryImpl();


    private ActrisMapper actrisMapper = new ActrisMapper();


    @Override
    public List<ActrisPreviewDto> findAll() {
        List<Actris> found = actrisRepository.findAll();

        System.out.println("ActrisServiceImpl -> found " + found.size() + " actris");

        return actrisMapper.mapToDtoList(found);
    }

    @Override
    public ActrisFullDto findById(Long id) {
        Actris found = actrisRepository.findById(id);

        System.out.println("ActrisServiceImp -> found: " + found);

        return actrisMapper.mapToDto(found);
    }

    @Override
    public ActrisFullDto create(ActrisCreateDto createDto) {
        Actris toSave = actrisMapper.mapToEntity(createDto);

        Actris created = actrisRepository.create(toSave);

        System.out.println("ActrisServiceImpl -> created actris: " + created);

        return actrisMapper.mapToDto(created);
    }

    @Override
    public ActrisFullDto update(ActrisUpdateDto updateDto) {
        Actris toUpdate = actrisMapper.mapToEntity(updateDto);

        Actris existingEntity = actrisRepository.findById(updateDto.getId());

        toUpdate.setEmail(existingEntity.getEmail());

        Actris updated = actrisRepository.update(toUpdate);

        System.out.println("ActrisServiceImpl -> updated actris: " + updated);

        return actrisMapper.mapToDto(updated);
    }

    @Override
    public void deleteById(Long id) {
        actrisRepository.deleteById(id);
        System.out.println("AcrtisServiceImpl -> actris with id " + id + " was deleted");
    }
}
