package com.kochetkov.pornoXXXvideo.service.serviceImpl;

import com.kochetkov.pornoXXXvideo.dto.administratorDto.AdministratorCreateDto;
import com.kochetkov.pornoXXXvideo.dto.administratorDto.AdministratorFullDto;
import com.kochetkov.pornoXXXvideo.dto.administratorDto.AdministratorPreviewDto;
import com.kochetkov.pornoXXXvideo.dto.administratorDto.AdministratorUpdateDto;
import com.kochetkov.pornoXXXvideo.entity.Administrator;
import com.kochetkov.pornoXXXvideo.exception.InvalidDtoException;
import com.kochetkov.pornoXXXvideo.exception.MissedUpdateldException;
import com.kochetkov.pornoXXXvideo.mapper.AdministratorMapper;
import com.kochetkov.pornoXXXvideo.repository.AdministratorRepository;
import com.kochetkov.pornoXXXvideo.service.AdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdministratorServiceImpl implements AdministratorService {

    @Autowired
    private AdministratorRepository administratorRepository;

     AdministratorMapper administratorMapper = new AdministratorMapper();

    @Override
    public AdministratorFullDto findByEmail(String email) {
        Administrator found = administratorRepository.findByEmail(email);
        return administratorMapper.mapToDto(found);
    }

    @Override
    public List<AdministratorPreviewDto> findAll() {
        List<Administrator> found = administratorRepository.findAll();
        return administratorMapper.mapToDtoList(found);
    }

    @Override
    public AdministratorFullDto findById(Long id) {
        Administrator found = administratorRepository.findById(id);
        return administratorMapper.mapToDto(found);
    }

    @Override
    public AdministratorFullDto create(AdministratorCreateDto createDto) {
        Administrator toSave = administratorMapper.mapToEntity(createDto);

        Administrator created = administratorRepository.create(toSave);

        return administratorMapper.mapToDto(created);
    }

    @Override
    public AdministratorFullDto update(AdministratorUpdateDto updateDto)  {
        Administrator toUpdate = administratorMapper.mapToEntity(updateDto);

        Administrator existingEntity = administratorRepository.findById(updateDto.getId());

        toUpdate.setRole(existingEntity.getRole());

        Administrator updated = administratorRepository.update(toUpdate);

        return administratorMapper.mapToDto(updated);
    }

    @Override
    public void deleteById(Long id) {
        administratorRepository.deleteById(id);
        System.out.println("AdministratorServiceImpl -> administrator with id " + id + " was deleted");
    }
}
