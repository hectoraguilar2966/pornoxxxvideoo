package com.kochetkov.pornoXXXvideo.service.serviceImpl;

import com.kochetkov.pornoXXXvideo.dto.actrisDto.ActrisLoginDto;
import com.kochetkov.pornoXXXvideo.entity.Actris;
import com.kochetkov.pornoXXXvideo.repository.ActrisRepository;
import com.kochetkov.pornoXXXvideo.repository.repositoryImpl.ActrisRepositoryImpl;
import com.kochetkov.pornoXXXvideo.service.AutActrisService;
import org.springframework.beans.factory.annotation.Autowired;

public class AutActrisServisImpl implements AutActrisService {

    private Actris loginedActris;

    @Autowired
    private ActrisRepository actrisRepository = new ActrisRepositoryImpl();

    @Override
    public void login(ActrisLoginDto loginDto) {
        Actris foundActris = actrisRepository.findByEmail(loginDto.getEmail());
        if(foundActris == null) {
            throw new RuntimeException("Actris not found by email: " + loginDto.getEmail());
        }

        if(!foundActris.getPassword().equals(loginDto.getPassword())) {
            throw new RuntimeException("Trainer password is incorrect");
        }

        loginedActris = foundActris;
    }

    @Override
    public void logOut() {
        loginedActris = null;
    }

    @Override
    public boolean isAuthenticated() {
        return loginedActris != null;
    }

    @Override
    public Actris getLoginedTrainer() {
        return loginedActris;
    }
}
