package com.kochetkov.pornoXXXvideo.service;

import com.kochetkov.pornoXXXvideo.dto.commentDto.CommentCreateDto;
import com.kochetkov.pornoXXXvideo.dto.commentDto.CommentFullDto;
import com.kochetkov.pornoXXXvideo.dto.commentDto.CommentPreviewDto;
import com.kochetkov.pornoXXXvideo.dto.commentDto.CommentUpdateDto;
import com.kochetkov.pornoXXXvideo.exception.InvalidDtoException;
import com.kochetkov.pornoXXXvideo.exception.MissedUpdateldException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CommentService {

    List<CommentPreviewDto> findAll();

    CommentFullDto findById(Long id);

    CommentFullDto create(CommentCreateDto createDto);

    CommentFullDto update(CommentUpdateDto updateDto);

    List<CommentPreviewDto> findAllByActrisId(Long id);

    void deleteById(Long id);
}
